import openrtb.default_params as default_params


class Ad(object):

    def __init__(self):
        self.bcat = []  # blocked advertiser categories
        self.badv = []  # block list of advertisers based on their domain (e.g: boom.com)
        self.bapp = []  # block list of apps, irrelevant right now
        self.creative = Creative()
        self.wlang = []  # white list of ad languages, ommission implies no restrictions
        self.events = []  # events allowed to be tracked, array of Event objects NOTE: we leave this unused right now because we do the tracking ourselves/e
        self.secure = 1  # Flag to indicate if the impression requires secure HTTPS URL creative assets and markup, where 0 = non-secure, 1 = secure
        self.rewarded = 0  # indicates whether the ad is being rendered as part of a rewarded/incentivized user experience
        self.ps = ''  # publisher signature, more on that in the security part


class Creative(object):

    def __init__(self):
        self.battr = []  # blocked creative

        self.video = Video()
        self.display = None
        self.audio = None

        self.dspmgr = None  # Name of ad mediation partner, SDK technology, or player responsible for rendering ad (typically video or mobile). Used by some ad servers to customize ad code by partner. Recommended for video and/or apps.
        self.dspmgrver = None  # above

        self.clktype = ''  # the click type see  ClickTypes


class Video(object):

    def __init__(self):
        self.subtype = ''  # see CreativeSubtypes
        self.mindur = default_params.VIDEO_AD_MIN_DURATION  # minimum video ad duration in seconds
        self.maxdur = default_params.VIDEO_AD_MAX_DURATION  # maximum video ad duration in seconds
        self.protocols = []  # see VideoProtocols - at least one must be provided

        self.w = None  # width of the video player in device independent pixels
        self.h = None  # height of the video player in device independent pixels
        self.startdelay = 0  # the start delay for preroll, midroll and postroll ads
        self.placement = ''  # placement of the impression see ImpressionPlacements TODo it seems like they forgot this in Adcom
        self.linearity = ''  # the linearity of the video ad that is allowed. see VideoLinearity TOODo
        self.skip = default_params.VIDEO_AD_SKIPPABILITY  # whether the ad is skippable
        self.skipmin = default_params.VIDEO_AD_MIN_DURATION_FOR_SKIPPABILITY
        self.skipafter = default_params.VIDEO_AD_MIN_DURATION_BEFORE_SKIP
        self.maxextended = 0
        self.minbitrate = default_params.VIDEO_AD_MIN_BITRATE  # minimum bitrate in kbps
        self.maxbitrate = default_params.VIDEO_AD_MAX_BITRATE
        self.playbackend = None  # the event that causes playback to end. TODo we don't need the events here right now cause we do the tracking ourselves
        self.mimes = []  # mime types allowed - 'video/mp4', 'application/javascript', ...


class Event(object):

    def __init__(self):
        self.event = 1  # the event code, probably self assigned
        self.methods = []  # array of event tracking methods available see EventTrackingMethods TODo we don't use these events right now
