class Site(object):

    def __init__(self):
        self.id = ''  # exchange specific id of the site
        self.name = ''  # name of the site, pre-agreed with the exchange
        self.domain = ''  # domain of teh site
        self.cat = []  # categories of the site, see adcom.constants.Categories
        self.publisher = Publisher()
        self.content = Content()
        self.keywords = ''  # comma separated list of keywords


class Publisher(object):

    def __init__(self) -> None:
        self.id = ''  # exchange specific publisher id
        self.name = ''  # exchange specific name of the publisher
        self.cat = []  # categories for the publisher see adcom.constants.Categories
        self.domain = ''  # highest level domain for the publisher


class Content(object):

    def __init__(self) -> None:
        self.id = ''  # exchange specific id uniquely identifying the content
        self.title = ''
        self.cat = []  # categories of the content, see adcom.constants.Categories
        self.keywords = ''  # comma separated list of keywords for the content


class Device(object):

    def __init__(self):
        self.geo = Geo()
        self.ipv4 = ''
        self.ipv6 = ''
        self.devicetype = ''  # see adcom.constants.DeviceTypes
        self.h = 0
        self.w = 0
        self.js = 1  # support for javascript
        self.macsha1 = ''  # mac address of the device in sha1
        self.macmd5 = ''  # mac address in md5


class Geo(object):

    def __init__(self) -> None:
        self.lat = 0.0
        self.lon = 0.0
        self.country = ''
        self.city = ''


class User(object):

    def __init__(self) -> None:
        self.id = ''  # supply side id of the user, see below self.buyerid
        self.buyeruid = ''  # demand-side id of the user, mapped by the exchange
        self.yob = 1001  # birth year of the user
        self.gender = ''  # gender of the user, see adcom.constants.Genders
        self.keywords = ''  # comma separated keywords
        self.geo = Geo()
