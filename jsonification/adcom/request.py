from marshmallow import fields
from jsonification.common import Schema

from openrtb.adcom.request import *
class VideoSchema(Schema):
    """
    all fields included
    """
    subtype = fields.Str(missing=None)
    mindur = fields.Int(missing=None)
    maxdur = fields.Int(missing=None)
    protocols = fields.List(fields.Str(), missing=None)
    w = fields.Int(missing=None)
    h = fields.Int(missing=None)
    startdelay = fields.Int(missing=None)
    placement = fields.Str(missing=None)
    linearity = fields.Str(missing=None)
    skip = fields.Int(missing=None)
    skipmin = fields.Int(missing=None)
    skipafter = fields.Int(missing=None)
    maxextended = fields.Int(missing=None)
    minbitrate = fields.Int(missing=None)
    maxbitrate = fields.Int(missing=None)
    mimes = fields.List(fields.Str(), missing=None)

    def get_base_instance(self):
        return Video()


class CreativeSchema(Schema):
    """
    missing fields: display, audio, dspmgr, dspmgrver, clktype
    """
    battr = fields.List(fields.Str(), missing=None)
    video = fields.Nested(VideoSchema, missing=None)

    def get_base_instance(self):
        return Creative()


class EventSchema(Schema):
    """
    all fields included
    """
    event = fields.Int(missing=None)
    methods = fields.List(fields.Int(), missing=None)

    def get_base_instance(self):
        return Event()


class AdSchema(Schema):
    """
    missing fields: wlang, secure, rewarded, ps
    """
    bcat = fields.List(fields.Str(), missing=None)
    badv = fields.List(fields.Str(), missing=None)
    bapp = fields.List(fields.Str(), missing=None)
    creative = fields.Nested(CreativeSchema, missing=None)
    events = fields.Nested(EventSchema, many=True, missing=None)

    def get_base_instance(self):
        return Ad()

