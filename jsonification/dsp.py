from marshmallow import fields, Schema, post_load
from persistence import Campaign, CampaignAd
from jsonification.adcom.response import AdSchema
import pdb

class CampaignAdSchema(Schema):
    adcom = fields.Nested(AdSchema, many=False)

    @post_load
    def create_instance(self, data):
        if 'adcom' not in data:
            raise Exception('adcom is the only required field in CampaignAd Objects and should be included')
        return CampaignAd(None, data['adcom'])


class CampaignSchema(Schema):
    id = fields.Str(required=True)
    seat = fields.Str(required=True)
    ads = fields.Nested(CampaignAdSchema, many=True)
    advertiser_domain = fields.Str()
    advertiser_categories = fields.List(fields.Str(), missing=None)
    bid_price = fields.Int(required=True)
    site_domains = fields.List(fields.Str(), missing=[])
    site_domains_black_list = fields.Bool(missing=True)
    site_categories = fields.List(fields.Str(), missing=[])
    site_categories_black_list = fields.Bool(missing=True)
    video_categories = fields.List(fields.Str(), missing=[])
    video_categories_black_list = fields.Bool(missing=True)
    enabled = fields.Bool(missing=True)

    @post_load
    def create_instance(self, data):
        campaign = Campaign(**data)
        for ad in data['ads']:
            ad.set_campaign(campaign)
        return campaign
