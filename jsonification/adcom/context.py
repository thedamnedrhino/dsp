from marshmallow import fields
from jsonification.common import Schema

from openrtb.adcom.context import *


class GeoSchema(Schema):
    """
    all fields mapped
    """
    lat = fields.Float(missing=None)
    lon = fields.Float(missing=None)
    country = fields.Str(missing=None)
    city = fields.Str(missing=None)

    def get_base_instance(self):
        return Geo()


class PublisherSchema(Schema):
    """
    all fields mapped
    """
    id = fields.Str(missing=None)
    name = fields.Str(missing=None)
    cat = fields.List(fields.Str())
    domain = fields.Str(missing=None)

    def get_base_instance(self):
        return Publisher()


class ContentSchema(Schema):
    """
    all fields mapped
    """
    id = fields.Str(missing=None)
    title = fields.Str(missing=None)
    cat = fields.List(fields.Str())
    keywords = fields.Str(missing=None)

    def get_base_instance(self):
        return Content()


class SiteSchema(Schema):
    """
    all fields mapped
    """
    id = fields.Str(missing=None)
    name = fields.Str(missing=None)
    domain = fields.Str(missing=None)
    cat = fields.List(fields.Str(), missing=[], allow_none=True)
    publisher = fields.Nested(PublisherSchema, allow_none=True, missing=None)
    content = fields.Nested(ContentSchema, allow_none=True, missing=None)
    keywords = fields.Str(allow_none=False)

    def get_base_instance(self):
        return Site()


class DeviceSchema(Schema):
    """
    all fields mapped
    """
    geo = fields.Nested(GeoSchema, missing=None)
    ipv4 = fields.Str(missing=None)
    ipv6 = fields.Str(missing=None)
    devicetype = fields.Str(missing=None)
    h = fields.Int(missing=None)
    w = fields.Int(missing=None)
    js = fields.Int(missing=None)
    macsha1 = fields.Str(missing=None)
    macmd5 = fields.Str(missing=None)

    def get_base_instance(self):
        return Device()


class UserSchema(Schema):
    """
    all fields mapped
    """
    city = fields.Str(missing=None)
    buyeruid = fields.Str(missing=None)
    yob = fields.Int(missing=None)
    gender = fields.Str(missing=None)
    keywords = fields.Str(missing=None)
    geo = fields.Nested(GeoSchema, missing=None)

    def get_base_instance(self):
        return User()
