from typing import Iterable, Collection, Set, Optional, Dict, Union, List
import openrtb.adcom.context as adcom_context
import openrtb.adcom.response as adcom_response
import openrtb.adcom.request as adcom_request
import openrtb.openrtb as ortb
import uuid
import pdb


class SiteDomainNormalizer():
    def normalize(self, domain: str):
        return domain #todo


class WrappedORTBItem():
    def __init__(self, original: ortb.Item, domain_normalizer: Optional[SiteDomainNormalizer]=None):
        self.original = original
        self.domain_normalizer = domain_normalizer if domain_normalizer is not None else SiteDomainNormalizer()

    def id(self):
        return self.original.id

    def get_video_categories(self):
        c = self.adcom_content()
        if c is None:
            return None
        return c.cat

    def allows_ad_to_bid(self, ad: 'CampaignAd'):
        return self._categories_are_allowed(ad) \
            and self._domain_is_allowed(ad)

    def _categories_are_allowed(self, ad: 'CampaignAd'):
        categories = self._adcom_ad().bcat
        if categories is None or len(categories) == 0:
            return True
        requirement = AdvertiserCategoriesRequirement(categories, True)
        return requirement.allows(None, ad)

    def _domain_is_allowed(self, ad: 'CampaignAd'):
        blocked_domains = self._adcom_ad().badv
        if blocked_domains is None or len(blocked_domains) == 0:
            return True
        requirement = AdvertiserDomainRequirement(blocked_domains, True, self.domain_normalizer)
        return requirement.allows(None, ad)

    def _adcom_ad(self) -> adcom_request.Ad:
        if self.original.domain.ad is None:
            raise Exception('each item should contain and adcom Ad object in the \'ad\' key of its domain property')
        return self.original.domain.ad

    def adcom_site(self) -> Optional[adcom_context.Site]:
        return self.original.domain.site

    def adcom_content(self) -> Optional[adcom_context.Content]:
        return self.adcom_site().content if self.adcom_site() is not None else None



class WrappedORTBRequest():
    def __init__(self, original: ortb.BidRequest):
        self.original = original

    def allows_campaign_to_bid(self, c: 'Campaign'):
        return True

    def get_site_domain(self) -> Optional[str]:
        if self._get_site() is None:
            return None

        return self._get_site().domain

    def request_id(self):
        return self.original.id

    def get_site_categories(self) -> Optional[Collection[str]]:
        if self._get_site() is None:
            return None

        return self._get_site().cat

    def get_video_categories(self) -> Optional[Collection[str]]:
        # NOTE: this is not used right now, the context is also provided in each item and we use that
        if self._get_content() is None:
            return None

        return self._get_content().cat

    def adcom_site(self) -> Optional[adcom_context.Site]:
        return self._get_site()

    def adcom_content(self) -> Optional[adcom_context.Content]:
        return self._get_content()

    def _get_site(self) -> Optional[adcom_context.Site]:
        return self.get_item().adcom_site()

    def _get_content(self) -> Optional[adcom_context.Content]:
        return self.get_item().adcom_content()

    def get_item(self):
        # ASSUMPTION only a single item in offer
        return WrappedORTBItem(self.original.offer.item[0])


class WrappedORTBResponse():
    def __init__(self, request: 'WrappedORTBRequest'):
        self.request = request
        self.bids = []

    def ORTBResponse(self) -> ortb.BidResponse:
        response = ortb.BidResponse(self.request.request_id(), self.generate_id())
        if len(self.bids) == 0:
            return self.no_bid(response.id, response.bidid)
        response.seatbid = self._generate_seat_bids()
        return response

    def _generate_seat_bids(self):
        seat_bids = []
        # ASSUMPTION each seat has only one campaign running
        for campaign_bid in self.bids: # type: CampaignBid
            seat_bids.append(campaign_bid.generate_ortb_seat_bid())
        return seat_bids

    def generate_id(self):
        return uuid.uuid1()

    def add_campaign_bid(self, bid: 'CampaignBid'):
        self.bids.append(bid)

    def no_bid(self, request_id, bid_id):
        return NoBidORTBResponse(request_id, bid_id)


class NoBidORTBResponse(ortb.BidResponse):
    pass

class Requirements():
    def __init__(self, requirements: Iterable['Requirement']):
        self.requirements = requirements

    def allows(self, context, video) -> bool:
        for r in self.requirements:
            if not r.allows(context, video):
                return False
        return True

class BlackWhiteRequirements(Requirements):

    def __init__(self, black_lists: Iterable['ListRequirement'], white_lists: Iterable['ListRequirement']):
        self.black_lists = black_lists
        self.white_lists = white_lists


    def allows(self, context, video) -> bool:
        for w in self.white_lists:
            if w.allows(context, video):
                return True
        for b in self.black_lists:
            if not b.allows(context, video):
                return False
        return True

class Requirement():
    def allows(self, context, video):
        pass # ABSTRACT


class ListRequirement(Requirement):
    def __init__(self, the_list: Collection[str], black_list: bool=True):
        self.the_list = the_list
        self.black_list = black_list
        self.requirement_checker = BlackListRequirementChecker() if black_list else WhiteListRequirementChecker()

    def allows(self, context, video):
        attrs = self.extract_attrs(context, video)
        if attrs is None:
            return True
        return self.requirement_checker.meets_requirement(set(self.the_list), set(attrs))

    def extract_attrs(self, context, video) -> Iterable[str]:
        pass # ABSTRACT


class DomainRequirement(ListRequirement):
    def  __init__(self, domains: Collection[str], black_list: bool=True, normalizer: Optional[SiteDomainNormalizer]=None):
        self.normalizer = normalizer if normalizer is not None else SiteDomainNormalizer()
        domains = [self.normalize_domain(d) for d in domains]
        super().__init__(domains, black_list)

    def normalize_domain(self, domain: str) -> str:
        return self.normalizer.normalize(domain)


class SiteDomainRequirement(DomainRequirement):
    def extract_attrs(self, context: Optional[adcom_context.Site], video=None):
        if context is not None:
            domain = context.domain
        else:
            return None
        return [self.normalize_domain(domain)]



class SiteCategoriesRequirement(ListRequirement):
    def extract_attrs(self, context: Optional[adcom_context.Site], video=None):
        if context is None:
            return None
        return context.cat


class VideoCategoriesRequirement(ListRequirement):
    def extract_attrs(self, context, video: Optional[adcom_context.Content]):
        if video is None:
            return None
        return video.cat


class AdvertiserCategoriesRequirement(ListRequirement):
    def extract_attrs(self, context, video: 'CampaignAd'):
        if video is None:
            return None
        return video.get_advertiser_categories()


class AdvertiserDomainRequirement(DomainRequirement):
    def extract_attrs(self, context, video: Optional['CampaignAd']):
        if video is None:
            return None
        if video.get_advertiser_domain():
            return [self.normalize_domain(video.get_advertiser_domain())]
        else:
            return None


class ListRequirementChecker():
    def meets_requirement(self, attrs: Set[str], request_attrs: Set[str]):
        pass


class WhiteListRequirementChecker(ListRequirementChecker):
    def meets_requirement(self, attrs: Set[str], request_attrs: Set[str]):
        return len(attrs.intersection(request_attrs)) > 0


class BlackListRequirementChecker(ListRequirementChecker):
    def meets_requirement(self, attrs: Set[str], request_attrs: Set[str]):
        return len(attrs.intersection(request_attrs)) == 0


class CampaignAd():
    def __init__(self, campaign: Optional['Campaign'], adcom: Union[Dict, adcom_response.Ad]):
        self.campaign = campaign
        self.requirements = Requirements([]) # we don't enforce any requirements on particular ads right now
        self.adcom = adcom

    def get_advertiser_categories(self):
        return self.campaign.get_advertiser_categories()

    def get_advertiser_domain(self):
        return self.campaign.get_advertiser_domain()

    def place_bid(self, campaign_bid: 'CampaignBid', request: WrappedORTBRequest):
        # ASSUMPTION single item in bid request
        item = request.get_item()
        if not self.requirements.allows(item.adcom_site(), item.adcom_content()):
            return
        if not item.allows_ad_to_bid(self):
            return

        ad_bid = AdBid(self, item)

        campaign_bid.bid_ad(ad_bid)

    def bid_price(self):
        return self.campaign.bid_price

    def campaign_id(self):
        return self.campaign.id

    def get_adcom(self):
        return self.adcom




class CampaignBid():
    def __init__(self, campaign: 'Campaign'):
        self.campaign = campaign
        self.ads = campaign.ads
        self.ad_bid = None # type: AdBid

    def seat_name(self):
        return self.campaign.seat_name()

    def initialize_bid(self, request: WrappedORTBRequest):
        for ad in self.ads:
            ad.place_bid(self, request)
            if self.ad_bid is not None:
                break

    def generate_ortb_seat_bid(self):
        seat_bid = ortb.Seatbid()
        seat_bid.seat = self.seat_name()
        # ASSUMPTION each request contains only one impression spot hence each campaign places a single bid only
        bids = [self.ad_bid.generate_ortb_bid()]
        seat_bid.bid = bids
        return seat_bid

    def has_bid(self) -> bool:
        return self.ad_bid is not None

    def place_bid(self, response: 'WrappedORTBResponse'):
        if not self.has_bid():
            return False
        response.add_campaign_bid(self)

    def placed_bid(self) -> bool:
        return self.has_bid()

    def bid_ad(self, bid: 'AdBid'):
        self.ad_bid = bid


class AdBid():
    def __init__(self, ad: 'CampaignAd', item: 'WrappedORTBItem'):
        self.ad = ad
        self.item = item

    def generate_ortb_bid(self):
        bid = ortb.Bid(self.item.id())
        bid.item = self.item.id()
        bid.price = self.ad.bid_price()
        bid.id = uuid.uuid1()
        bid.cid = self.ad.campaign_id()
        domain = ortb.BidDomain()
        domain.ad = self.ad.get_adcom()
        bid.domain = domain
        return bid



class Campaign():
    def __init__(self, id: str, seat: str, bid_price: int, ads: List[CampaignAd], advertiser_domain: str, advertiser_categories: Collection[str], site_domains: Collection[str] = (), site_domains_black_list: bool=True, site_categories: List[str]=[],
                 site_categories_black_list: bool=True, video_categories: List[str]=[], video_categories_black_list: bool=True, enabled=True):
        self.id = id
        self.seat = seat
        self.ads = []
        for ad in ads:
            self.add_ad(ad)
        self.advertiser_domain = advertiser_domain
        self.advertiser_categories = advertiser_categories
        self.bid_price = bid_price
        self.site_domains = site_domains
        self.site_domains_black_list = site_domains_black_list
        self.site_categories = site_categories
        self.site_categories_black_list = site_categories_black_list
        self.video_categories = video_categories
        self.video_categories_black_list = video_categories_black_list
        self.enabled = enabled
        self.initialize()

    def initialize(self):
        self.requirements = self._construct_requirements(self.site_domains, self.site_domains_black_list, self.site_categories, self.site_categories_black_list, self.video_categories, self.video_categories_black_list)

    def enable(self):
        self.enabled = True

    def disable(self):
        self.enabled = False

    def is_enabled(self):
        return self.enabled

    def add_ad(self, ad: CampaignAd):
        self.ads.append(ad)
        ad.campaign = self

    def get_advertiser_categories(self):
        return self.advertiser_categories

    def get_advertiser_domain(self):
        return self.advertiser_domain

    def seat_name(self):
        return self.seat

    def can_bid(self, request: WrappedORTBRequest):
        if not request.allows_campaign_to_bid(self):
            return False
        if not self.requirements.allows(request.adcom_site(), request.adcom_content()):
            return False
        return True

    def place_bid(self, request: WrappedORTBRequest, response: WrappedORTBResponse) -> bool:
        if not self.can_bid(request):
            return False
        campaign_bid = self.generate_campaign_bid()
        campaign_bid.initialize_bid(request)
        campaign_bid.place_bid(response)
        return campaign_bid.placed_bid()

    def generate_campaign_bid(self) -> CampaignBid:
        return CampaignBid(self)

    def _construct_requirements(self, site_domains, site_domains_bl, site_categories, site_categories_bl, video_categories, video_categories_bl):
        black_lists = []
        white_lists = []
        if len(site_domains) > 0:
            req_list = black_lists if site_domains_bl else white_lists
            req_list.append(SiteDomainRequirement(site_domains, site_domains_bl))
        if len(site_categories) > 0:
            req_list = black_lists if site_categories_bl else white_lists
            req_list.append(SiteCategoriesRequirement(site_categories, site_categories_bl))
        if len(video_categories) > 0:
            req_list = black_lists if video_categories_bl else white_lists
            req_list.append(VideoCategoriesRequirement(video_categories, video_categories_bl))
        requirements = BlackWhiteRequirements(black_lists, white_lists)
        return requirements

    def get_id(self):
        return self.id

class DSP():
    def __init__(self, campaigns: List[Campaign]):
        self.campaigns = []
        for campaign in campaigns:
            self.new_campaign(campaign)

    def bid(self, bid_request: WrappedORTBRequest) -> WrappedORTBResponse:
        response = WrappedORTBResponse(bid_request)
        for c in self.campaigns:
            if c.is_enabled():
                c.place_bid(bid_request, response)
        return response

    def new_campaign(self, c: Campaign):
        self.campaigns.append(c)
        return c

    def delete_campaign(self, campaign_id: str):
        c = self._get_campaign_or_raise_error(campaign_id)
        self.campaigns.remove(c)
        return c

    def enable_campaign(self, campaign_id: str):
        c = self._get_campaign_or_raise_error(campaign_id)
        c.enable()
        return c

    def disable_campaign(self, campaign_id: str):
        c = self._get_campaign_or_raise_error(campaign_id)
        c.disable()
        return c

    def _get_campaign_or_raise_error(self, campaign_id: str):
        for c in self.campaigns:
            if c.id == campaign_id:
                return c
        raise Exception('there is no campaign with the id "{}"'.format(campaign_id))
