from typing import Dict, List, Tuple, Union
import pytest
from flask.testing import FlaskClient
from rom import util
import flask.wrappers
import flask_app.dsp
from persistence import Campaign
from jsonification.openrtb import OpenRtbSchema
from openrtb.openrtb import BidResponse
import openrtb.adcom.response
import unittest


@pytest.fixture
def client():
    util.set_connection_settings(port='3000')
    util.CONNECTION.flushdb()

    app = flask_app.dsp.app
    configure_app(app)
    return app.test_client()

def configure_app(app):
    app.testing = True
    app.config['REDIS_PORT'] = 3000

class TestDspRest():

    def test_add_single_campaign_detailed(self, client: FlaskClient):
        self.assert_there_are_no_campaigns()
        campaign_dict = self._get_campaigns()[0]
        self.add_campaign_from_dict(campaign_dict, client)
        self.assert_there_are_N_campaigns(1)
        campaign = self._get_single_campaign_in_redis()
        self.validate_campaign_against_dictionary(campaign, campaign_dict)

    def test_add_campaigns(self, client: FlaskClient):
        self.assert_there_are_no_campaigns()
        self.add_campaigns(client)
        self.assert_campaigns_are_added()

    def test_add_campaigns_using_multiple_clients(self, client):
        self.assert_there_are_no_campaigns()
        campaigns = self._get_campaigns()
        client2 = self._get_second_client()
        assert client2 is not client
        self.add_campaign_from_dict(campaigns[0], client)
        self.add_campaign_from_dict(campaigns[1], client2)
        self.assert_there_are_N_campaigns(2)

    def test_bid_request(self, client: FlaskClient):
        campaign_dicts = self.add_campaigns(client)
        bid_response = self.send_bid_request(client)
        self.validate_bid_response(bid_response)

    def test_get_campaign(self, client: FlaskClient):
        self.add_campaigns(client)
        self.request_and_validate_campaign(self._get_non_blocked_campaign(), client)
        self.request_and_validate_campaign(self._get_blocked_campaign(), client)

    def test_get_campaign_list(self, client: FlaskClient):
        campaigns = self.add_campaigns(client)
        ids = [c['id'] for c in campaigns]
        response = client.get('/campaign/')
        retrieved_ids = response.get_json(force=True)
        assert len(ids) == len(retrieved_ids)
        for id in ids:
            assert id in retrieved_ids
        assert response.status_code == 200

    def test_get_enabled_campaigns_list(self, client: FlaskClient):
        c1, c2 = self._get_campaigns()
        c2['enabled'] = False
        self.add_campaign_from_dict(c1, client)
        self.add_campaign_from_dict(c2, client)
        self.assert_there_are_N_campaigns(2)
        response = client.get('/campaign/enabled')
        ids = response.get_json()
        assert len(ids) == 1
        assert c1['id'] in ids
        assert c2['id'] not in ids
        assert response.status_code == 200

    def test_enabling_disabling_campaign(self, client: FlaskClient):
        campaigns = self.add_campaigns(client)
        self.assert_there_are_N_campaigns(len(campaigns))
        campaign_dict = campaigns[0]
        id1 = campaign_dict['id']
        id2 = campaigns[1]['id']
        for id in (id1, id2):
            self.assert_campaign_is_enabled(id, client)
        self.disable_campaign(id1, client)
        self.assert_there_are_N_campaigns(len(campaigns)) # check that no new campaigns are created by mistake during this update
        self.assert_campaign_is_disabled(id1, client)
        campaign_dict['enabled'] = False
        self.request_and_validate_campaign(campaign_dict, client) # check that nothing else has changed
        self.assert_campaign_is_enabled(id2, client)
        self.enable_campaign(id1, client)
        self.assert_there_are_N_campaigns(len(campaigns))
        for id in (id1, id2):
            self.assert_campaign_is_enabled(id, client)
        campaign_dict['enabled'] = True
        self.request_and_validate_campaign(campaign_dict, client)
        
        
    def test_delete_campaign(self, client: FlaskClient):
        campaigns = self.add_campaigns(client)
        c = campaigns[0]
        self.assert_there_are_N_campaigns(len(campaigns))
        self.delete_campaign(c['id'], client)
        self.assert_there_are_N_campaigns(len(campaigns) - 1)
        self.assert_campaign_does_not_exist(c['id'], client)

    def send_bid_request(self, client: FlaskClient) -> flask.wrappers.Response:
        bid_request = {
            'id': 'request_id',
            'offer': {
                'item': [
                    {
                        'id': 'item_id', 'domain': {'ad': {'bcat': ['blocked_advertiser_category']}}
                     }
                ]
            }
        }
        open_rtb = {'request': bid_request}
        return client.post('/bid_request', json=open_rtb)

    def validate_bid_response(self, bid_response: flask.wrappers.Response):
        if not isinstance(bid_response, flask.wrappers.Response):
            raise Exception('wrong response class provided')
        response_json = bid_response.get_json(force=True)
        schema = OpenRtbSchema()
        ortb, errors = schema.load(response_json)
        assert len(errors) == 0
        assert ortb.request is None
        assert ortb.response is not None
        assert isinstance(ortb.response, BidResponse)
        self.validate_ortb_bid_response(ortb.response)

    def validate_ortb_bid_response(self, response: BidResponse):
        assert response.id == 'request_id'
        assert len(response.seatbid) == 1 # out of the two campaigns, only one is able to bid because the other has a blocked category
        seatbid = response.seatbid[0]
        campaign_dict = self._get_non_blocked_campaign()
        assert seatbid.seat == campaign_dict['seat']
        assert len(seatbid.bid) == 1
        bid = seatbid.bid[0]
        assert bid.item == 'item_id'
        assert bid.price == campaign_dict['bid_price']
        adcom_ad = bid.domain.ad
        self.validate_adcom_ad_adgainst_dictionary(adcom_ad, campaign_dict['ads'][0]['adcom'])


    def add_campaigns(self, client: FlaskClient):
        for campaign_dict in self._get_campaigns():
            self.add_campaign_from_dict(campaign_dict, client)
        return self._get_campaigns()

    def add_campaign_from_dict(self, campaign_dict, client: FlaskClient):
        response = client.post('/campaign/new', json=campaign_dict) # type: flask.wrappers.Response
        print(response)
        assert response.status_code == 200

    def delete_campaign(self, campaign_id, client: FlaskClient):
        response = client.post('/campaign/{}/delete'.format(campaign_id))
        assert response.status_code == 200
        
    def _get_single_campaign_in_redis(self):
        return Campaign.query.all()[0]

    def _get_non_blocked_campaign(self):
        campaign_dict1 = {
            'id': '1',
            'seat': '1',
            'advertiser_domain': 'google.com',
            'advertiser_categories': ['non_blocked_advertiser_category'],
            'bid_price': 10,
            'site_domains': ['netflix.com'],
            'site_domains_black_list': True,
            'site_categories': ['sport', 'dress'],
            'site_categories_black_list': True,
            'video_categories': ['sport', 'football'],
            'video_categories_black_list': True,
            'ads': [{'adcom': {'dadid': '5', 'cat': [], 'creative': {'video': {'subtype': 1}}}}],
            'enabled': True
        }
        return campaign_dict1

    def _get_blocked_campaign(self):
        campaign_dict = self._get_non_blocked_campaign()
        campaign_dict['id'] = '2'
        campaign_dict['seat'] = 'seat_2'
        campaign_dict['advertiser_categories'].append('blocked_advertiser_category')
        return campaign_dict

    def _get_campaigns(self):
        return [self._get_non_blocked_campaign(), self._get_blocked_campaign()]

    def assert_campaign_does_not_exist(self, campaign_id, client: FlaskClient):
        response = self.request_campaign(campaign_id, client)
        assert response.status_code >= 400

    def assert_campaigns_are_added(self):
        assert len(Campaign.query.all()) == len(self._get_campaigns())

    def assert_there_are_no_campaigns(self):
        assert len(Campaign.query.all()) == 0

    def assert_there_are_N_campaigns(self, N: int):
        assert len(Campaign.query.all()) == N

    def _get_second_client(self):
        import flask_app.dsp
        app = flask_app.dsp.app
        configure_app(app)
        return app.test_client()

    def request_and_validate_campaign(self, campaign_dict, client: FlaskClient):
        id = campaign_dict['id']
        response = self.request_campaign(id, client)
        assert response.status_code == 200
        returned_dict = response.get_json(force=True)
        self.assert_items_are_equal(campaign_dict, returned_dict)

    def request_campaign(self, campaign_id, client):
        return client.get('/campaign/{}'.format(campaign_id)) # type: flask.wrappers.Response

    def enable_campaign(self, campaign_id, client: FlaskClient):
        response = client.post('/campaign/{}/enable'.format(campaign_id))
        assert response.status_code == 200

    def disable_campaign(self, campaign_id, client: FlaskClient):
        response = client.post('/campaign/{}/disable'.format(campaign_id))
        assert response.status_code == 200

    def assert_campaign_is_enabled(self, campaign_id, client: FlaskClient, enabled=True):
        response = client.get('/campaign/{}'.format(campaign_id))
        assert response.status_code == 200
        campaign = response.get_json(force=True)
        assert enabled == campaign['enabled']

    def assert_campaign_is_disabled(self, campaign_id, client: FlaskClient):
        self.assert_campaign_is_enabled(campaign_id, client, enabled=False)

    def validate_campaign_against_dictionary(self, c: Campaign, campaign_dictionary: Dict):
        assert c.id == campaign_dictionary['id']
        assert c.seat == campaign_dictionary['seat']
        assert c.bid_price == campaign_dictionary['bid_price']
        assert c.advertiser_domain == campaign_dictionary['advertiser_domain']
        assert c.advertiser_categories == campaign_dictionary['advertiser_categories']
        assert c.site_domains == campaign_dictionary['site_domains']
        assert c.site_domains_black_list == campaign_dictionary['site_domains_black_list']
        ads1 = c.ads
        ads2 = campaign_dictionary['ads']
        assert len(ads1) == len(ads2)
        ad = ads1[0]
        ad_dict = ads2[0]
        self.validate_adcom_ad_adgainst_dictionary(ad.adcom, ad_dict['adcom'])

    def validate_adcom_ad_adgainst_dictionary(self, adcom_ad: openrtb.adcom.response.Ad, adcom_ad_dict: Dict):
        assert adcom_ad.dadid == adcom_ad_dict['dadid']
        assert adcom_ad.creative.video.subtype == adcom_ad_dict['creative']['video']['subtype']

    def assert_items_are_equal(self, item1, item2):
        assert type(item1) == type(item2)
        if isinstance(item1, (list, tuple)):
            self.assert_lists_are_equal(item1, item2)
        elif isinstance(item1, dict):
            self.assert_dicts_are_equal(item1, item2)
        else:
            assert item1 == item2

    def assert_dicts_are_equal(self, dict1, dict2):
        for k, v1 in dict1.items():
            if bool(v1) or v1 is False:
                assert k in dict2
                v2 = dict2[k]
                self.assert_items_are_equal(v1, v2)
            else:
                assert k not in dict2 or not bool(dict2[k])
        # now the other way around so dict2 doesn't have additional non-falsey keys
        for k, v1 in dict2.items():
            if bool(v1) or v1 is False:
                assert k in dict1
                v2 = dict1[k]
                self.assert_items_are_equal(v1, v2)
            else:
                assert k not in dict2 or not bool(dict2[k])

    def assert_lists_are_equal(self, l1, l2):
        assert len(l1) == len(l2)
        for i in range(0, len(l1)):
            self.assert_items_are_equal(l1[i], l2[i])
