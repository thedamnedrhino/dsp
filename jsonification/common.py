import marshmallow

class Schema(marshmallow.Schema):
    def get_base_instance(self):
        pass # ABSTRACT
    @marshmallow.post_load
    def create_instance(self, data):
        obj = self.get_base_instance() # type: object
        for k, v in data.items():
            obj.__setattr__(k, v)
        return obj

