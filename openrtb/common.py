from enum import Enum


class ConstantList(object):

    def __init__(self, list, code='', name=''):
        if code:
            inverted = dict([[v, k] for k, v in list.items()])
            if code not in inverted:
                raise Exception('the code {} is not defined'.format(code))
            self.code = code
            self.name = inverted[code]
        elif name:
            if name not in list:
                raise Exception('the name {} is not defined'.format(name))
            self.code = list[name]
            self.name = name


class AuctionType(object):
    FIRST_PRICE = 1
    SECOND_PRICE = 2


class Currencies(object):
    IRR = 'IRR'
    USD = 'USD'


class Macros(object):
    REQUEST_ID = '${OPENRTB_ID}'
    BID_ID = '${OPENRTB_BID_ID}'
    ITEM_ID = '${OPENRTB_ITEM_ID}'
    SEAT_ID = '${OPENRTB_SEAT_ID}'  # seatbid.seat
    PRICE = '${OPENRTB_PRICE}'  # clearing price of the bid
    CURRENCY = '${OPENRTB_CURRENCY}'  # currency used in the bid (for confirmation only)
    MBR = '${OPENRTB_MBR}'  # market bid ratio defined as clearance price/bid price
    LOSS = '${OPENRTB_LOSS}'  # the loss reason code

    VALUE_PLACEHOLDER = 'AUDIT'  # this is used as the VALUE for the substitution in the exchange when the value of the macro is unknown
