import unittest
import copy
import dsp
import openrtb.adcom.context as adcom_context
import openrtb.openrtb as ortb
import openrtb.adcom.request as adcom_request


class CampaignRequirementsTest(unittest.TestCase):
    def get_test_campaign(self, domains=[], domains_bl=True, site_cat=[], site_cat_bl=True,
                          vid_cat=[], vid_cat_bl=True):
        return dsp.Campaign('test', 'test', 1, [], 'a.com', [], domains, domains_bl, site_cat,
                            site_cat_bl, vid_cat, vid_cat_bl)

    def test_requirements(self):
        c = self.get_test_campaign(domains=['a.com'], domains_bl=True, site_cat=['a'],
                                   site_cat_bl=True, vid_cat=['va'], vid_cat_bl=True)
        requirements = c.requirements
        allowed_site = adcom_context.Site()
        allowed_site.domain = 'b.com'
        allowed_site.cat = ['b', 'ab']
        domain_denied_site = adcom_context.Site()
        domain_denied_site.domain = 'a.com'
        cat_denied_site = adcom_context.Site()
        cat_denied_site.cat = ['b', 'a']
        self.assertTrue(requirements.allows(allowed_site, None))
        self.assertTrue(c.can_bid(self.ortb_request(allowed_site, None)))
        self.assertFalse(requirements.allows(domain_denied_site, None))
        self.assertFalse(c.can_bid(self.ortb_request(domain_denied_site, None)))
        self.assertFalse(requirements.allows(cat_denied_site, None))
        self.assertFalse(c.can_bid(self.ortb_request(cat_denied_site, None)))
        allowed_vid = adcom_context.Content()
        allowed_vid.cat = ['vb']
        denied_vid = adcom_context.Content()
        denied_vid.cat = ['vb', 'va']
        self.assertTrue(requirements.allows(allowed_site, allowed_vid))
        self.assertTrue(c.can_bid(self.ortb_request(allowed_site, allowed_vid)))
        self.assertFalse(requirements.allows(None, denied_vid))
        self.assertFalse(c.can_bid(self.ortb_request(None, denied_vid)))
        self.assertFalse(requirements.allows(allowed_site, denied_vid))
        self.assertFalse(c.can_bid(self.ortb_request(allowed_site, denied_vid)))

    def ortb_request(self, site, content):
        if site is None:
            site = adcom_context.Site()
        site.content = content
        request = ortb.BidRequest()
        request.offer = ortb.Offer()
        item = ortb.Item()
        domain = ortb.ItemDomain()
        domain.site = site
        item.domain = domain
        request.offer.item = [item]
        return dsp.WrappedORTBRequest(request)


class CampaignBidTest(unittest.TestCase):
    def test_non_empty_bid(self):
        c = self.create_campaign()
        request = self.create_bid_request()
        self.assertTrue(c.can_bid(request))
        response = self.create_container_response(request)
        c.place_bid(request, response)
        self.validate_wrapped_response(response)
        self.validate_response(response.ORTBResponse())

    def test_dsp_with_multiple_campaigns(self):
        c1 = self.create_campaign()
        c2 = copy.deepcopy(c1)
        c3 = self.create_blocked_campaign()
        campaigns = self.get_blocking_campaigns()
        campaigns.append(c1)
        campaigns.append(c2)

        d = dsp.DSP(campaigns)
        request = self.create_bid_request()
        response = d.bid(request).ORTBResponse()
        self.assertEqual(2, len(response.seatbid))
        for s in response.seatbid:
            self.assertEqual('seat', s.seat)
            self.assertEqual(1, len(s.bid))
            bid = s.bid[0]
            self.assertEqual(bid.item, 'item_id')
            self.assertEqual(bid.price, 10)

    def create_blocked_campaign(self):
        pass

    def test_blocked_bids(self):
        blocking_campaigns = self.get_blocking_campaigns()
        for b in blocking_campaigns:
            self.validate_blocking_campaign(b)

    def test_non_empty_white_overriding_black_list_bid(self):
        c = self.create_white_overriding_black_list_campaign()
        request = self.create_bid_request()
        self.assertTrue(c.can_bid(request))
        response = self.create_container_response(request)
        c.place_bid(request, response)
        self.validate_response(
            response.ORTBResponse())  # same as used in test non_empty_bid, we don't need to create something new as the campaigns are pretty much the same

    def validate_blocking_campaign(self, campaign: dsp.Campaign):
        request = self.create_bid_request()
        response = self.create_container_response(request)
        campaign.place_bid(request, response)
        self.assert_response_is_empty(response.ORTBResponse())

    def create_campaign(self) -> dsp.Campaign:
        campaign = dsp.Campaign('1', 'seat', 10, [], 'a.com', ['advertiser'], ['black.com'], True,
                                ['cat_1'])
        ad = dsp.CampaignAd(campaign, dict())
        campaign.add_ad(ad)
        return campaign

    def get_blocking_campaigns(self):
        # site domain black listing
        c1 = dsp.Campaign('c1', 'seat11', 10, [], 'advertiser_domain', ['advertiser_1'],
                          ['some_domain.com'], True)
        ad1 = dsp.CampaignAd(c1, dict())
        c1.add_ad(ad1)
        # video category black listing
        c2 = dsp.Campaign('c2', 'seat2', 10, [], 'advertiser_domain', ['advertiser_2'],
                          ['asdf.com'], True, ['non-effective-white-list'], False, ['mameh'])
        ad2 = dsp.CampaignAd(c2, dict())
        ad22 = copy.copy(ad1)
        ad22.campaign = c2
        c2.add_ad(ad1)

        c2.add_ad(ad2)
        # site category black listing
        cscbl = dsp.Campaign('cscbl', 'seat_cscbl', 30, [], 'advertiser_domain', [],
                             site_categories=['some_blocked_cat'])
        adcbl = copy.copy(ad1)
        adcbl.campaign = cscbl
        cscbl.add_ad(adcbl)

        # black listed advertiser domain
        c3 = dsp.Campaign('c3', 'seat3', 20, [], 'blocked_advertiser_domain', ['advertiser_3'])
        ad31 = copy.copy(ad1)
        ad31.campaign = c3
        c3.add_ad(ad31)

        # black listed advertiser category
        c4 = dsp.Campaign('c4', 'seat4', 30, [], 'advertiser_domain',
                          ['non-blocked advertiser category', 'blocked_advertiser_category'])
        ad41 = copy.copy(ad1)
        ad41.campaign = c4
        c4.add_ad(ad41)
        return [c1, c2, c3, c4, cscbl]

    def create_white_overriding_black_list_campaign(self):
        c1 = dsp.Campaign('c1', 'seat', 10, [], 'adomain', ['some_domain.com'],
                          site_categories=['not_blocked'], site_categories_black_list=False)
        ad1 = dsp.CampaignAd(c1, dict())
        c1.add_ad(ad1)
        return c1

    def create_bid_request(self) -> dsp.WrappedORTBRequest:
        r = ortb.BidRequest()
        r.id = 'request_id'
        ad = adcom_request.Ad()
        ad.bcat = ['blocked_advertiser_category', 'other_blocked_category']
        ad.badv = ['blocked_advertiser_domain', 'other_blocked_domain']
        creative = adcom_request.Creative()
        video = adcom_request.Video()
        creative.video = video
        offer = ortb.Offer()
        item = ortb.Item()
        item.id = 'item_id'
        domain = ortb.ItemDomain()
        domain.ad = ad
        site = adcom_context.Site()
        site.cat = ['not_blocked', 'some_blocked_cat']
        site.domain = 'some_domain.com'
        domain.site = site
        content = adcom_context.Content()
        content.cat = ['mameh']
        site.content = content
        offer.item = [item]
        r.offer = offer
        item.domain = domain
        return dsp.WrappedORTBRequest(r)

    def validate_wrapped_response(self, response: dsp.WrappedORTBResponse):
        pass

    def validate_response(self, response: ortb.BidResponse):
        self.assertEqual('request_id', response.id)
        bids = response.seatbid
        self.assertEqual(1, len(bids))
        bid = bids[0]  # type: ortb.Seatbid
        self.assertEqual('seat', bid.seat)
        self.assertEqual(1, len(bid.bid))
        bid = bid.bid[0]  # type: ortb.Bid
        self.assertEqual('item_id', bid.item)
        self.assertEqual(bid.price, 10)

    def assert_response_is_empty(self, response: ortb.BidResponse):
        self.assertEqual('request_id', response.id)
        bids = response.seatbid
        self.assertEqual(0, len(bids))
        self.assertTrue(isinstance(response, dsp.NoBidORTBResponse))

    def create_container_response(self,
                                  request: dsp.WrappedORTBRequest) -> dsp.WrappedORTBResponse:
        return dsp.WrappedORTBResponse(request)


if __name__ == '__main__':
    unittest.main()
