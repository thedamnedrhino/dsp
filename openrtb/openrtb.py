from enum import Enum
from openrtb.common import AuctionType, Currencies, ConstantList


class OpenRtb(object):

    def __init__(self, request: 'BidRequest' = None, response: 'BidResponse' = None):
        self.ver = '3.0'  # the openrtb version being used
        self.domainspec = 'AdCOM'
        self.domainver = '1.0'  # version of AdCOM used
        self.request = request
        self.response = response
        if request is not None and response is None:
            raise AssertionError('only one of request and response can be specified in an openrtb request/response')
        self.ext = {}  # exchange specific stuff


class BidRequest(object):

    def __init__(self):
        self.id = ''
        self.test = False  # indicator of test mode
        self.tmax = 1000  # maximum time in milliseconds to respond to this request
        self.at = AuctionType.FIRST_PRICE  # the auction type
        self.curs = [Currencies.IRR]  # white/black list of currencies for the bid, see self.wcurs
        self.wcurs = 1  # whether to whitelist or blacklist currencies in self.curs. provide 0 for blacklist
        self.seats = []  # white/black list of seats allowed to bid on the bid, see self.wlists
        self.wseats = 0  # whether to interpret self.seats as a white or black list, we default to black
        self.source = {}  # an Openrtb Source object that conveys data about the relevant authorities (what exchange makes the final decision) on the bid. probably irrelevant in our case
        self.offer = Offer()  # an offer
        self.domain = {}  # adcom object that provides data about the context of the item being sold (e.g: user, site, app)
        self.ext = {}  # extensions to openrtb we have none as of yet


class Offer(object):
    """
    represents the goods being auctioned in the request
    """

    def __init__(self):
        self.item = []  # set of the actual items (instances of Item) being sold
        self.package = 0  # whether the items in this offer represents all the available items in that page
        self.dburl = ''  # billing notice url called by the demand partner if the exchange becomes billable based on their negotiated billing policy with the ssp/exchange
        self.ext = {}


class Item(object):
    """
    represents a single item being offered, i.e: an impression
    """

    def __init__(self):
        self.id = '1'  # an id for this item in this context. can start from 1 and auto increment. is a string though.
        self.qty = 1  # number of identical instances of this item being sold, for us it should be almost always 1. but this should be studied further
        self.flr = 1.0  # minimum bid price expressed in CPM
        self.flrcur = Currencies.IRR  # the currency for the floor price above
        self.seq = 1  # the sequence number: if multiple items are included in the bidrequest, this allows for coordinated delivery (probably irrelevant in our use case)
        self.pmp = {}  # A Pmp object representing any private marketplace deals for this item
        self.domain = {}  # an instance of the AdCom Domain object, see adcom.context.site
        self.ext = {}


class ItemDomain(object):

    def __init__(self):
        self.ad = None # type: openrtb.adcom.request.Ad
        self.site = None # type: openrtb.adcom.context.Site


class BidDomain(object):

    def __init__(self):
        self.ad = None # type: openrtb.adcom.response.Ad


class Pmp(object):

    PUBLIC_OFFER = 0
    PRIVATE_OFFER = 1

    """
    object representing private marketplace deals for an item
    """

    def __init__(self):
        self.private = self.PUBLIC_OFFER  # flag indicating whether bids are accepted from everyone or or that they're restricted to the specified deals and terms thereof
        self.deal = []  # array of Deal objects
        self.ext = {}


class Deal(object):

    def __init__(self):
        self.id = ''  # the deal id of this pmp deal
        self.qty = None  # number of instances of this item this deal is applicable to, defaults to the parent Item.qty
        self.flr = 1.0  # minimum deal price for this item in CPM
        self.flrcur = Currencies.IRR  # the currency for self.flr
        self.at = None  # optional override of the exchanges auction type (BidRequest.at)
        self.seat = []  # WHITElist of buyers allowed to bid (ommission implies no restrictions)
        self.wadomain = []  # array of advertiser domains allowed to bid on this deal. ommission -> no restrictions
        self.ext = {}


class BidResponse(object):

    def __init__(self, request_id, bid_id):
        self.id = request_id  # id of the bid request that this is a response to
        self.bidid = bid_id  # bidder generated id of the response (individual 'bid's have their own ids) to help with tracking logging (SHOULD be able to be uniquely identifiable)
        self.nbr = None  # reason for not bidding if applicable from NoBidReasons
        self.cur = Currencies.IRR  # the currency for the bid
        self.cdata = ''  # allows the bidder to set data for the exchange's cookie (todo must figure out what this is used for)
        self.seatbid = []  # array of Seatbid objects
        self.ext = {}


class Seatbid(object):
    # see self.package
    PACKAGED = 1
    NOT_PACKAGED = 0

    def __init__(self):
        self.seat = ''  # id of the buyer on whose behalf this bid is made e.g: coca-cola or '1'
        self.package = Seatbid.NOT_PACKAGED  # flag for determining whether the buyer is only interested in the items only if they are all won together, as opposed to wanting each item individually regartdless of obtaining the other ones in the bid request
        self.bid = []  # array of Bid objects representing the bids made
        self.ext = {}


class Bid(object):

    def __init__(self, item_id):
        self.id = ''  # id of this individual bid
        self.item = ''  # id of the item object this bid is being made for (Item.id)
        self.deal = ''  # Reference to a deal from the bid request if this bid pertains to a private marketplace deal; specifically “deal.id”
        self.price = 1.0  # amount of the bid in CPM
        self.cid = ''  # id of the campaign that the ad in this bid relates to (can help with logging and tracking)
        self.tactic = ''  # unbelievably useless for now
        self.nurl = ''  # win notification url for this bid (not in openrtb 3.0 spec draft. but should be added later)
        self.burl = ''  # Billing notice URL called by the supply partner when a winning bid becomes billable based on exchange-specific business policy.  One of burl in the response or dburl in the request must be present (exception for VAST).
        self.lurl = ''  # Loss notice URL called by the supply partner when a bid is known to have been lost
        self.domain = {}  # an instance of the AdCom Domain object, see adcom.response.ad
        self.ext = {}


class NoBidReasons(ConstantList):
    NO_BID_REASONS = {
        'Unknown Error': 0,
        'Technical Error': 1
    }

    def __init__(self, code='', name=''):
        super().__init__(NoBidReasons.NO_BID_REASONS, code, name)
