from openrtb.common import ConstantList


class Genders(ConstantList):
    GENDERS = {
        'Male': 'M',
        'Female': 'F'
    }

    def __init__(self, code='', name=''):
        super().__init__(Genders.GENDERS, code, name)


class Categories(ConstantList):
    CATEGORIES = {
        'Arts & Entertainment': 'IAB1',
        'Books & Literature': 'IAB1-1'
    }

    def __init__(self, code='', name=''):
        super().__init__(Categories.CATEGORIES, code=code, name=name)


class CreativeAttributes(ConstantList):
    CREATIVE_ATTRIBUTES = {
        'Audio Ad (Auto-play)': 1,
        'Audio Ad (User Initiated)': 2,
        'In-Banner Video Ad (Auto-Play)': 6

    }

    def __init__(self, code='', name=''):
        super().__init__(CreativeAttributes.CREATIVE_ATTRIBUTES, code=code, name=name)


class CreativeSubtypes(ConstantList):
    CREATIVE_SUBTYPES = {
        'VAST 1.0': 1,
        'VAST 2.0': 2,
        'VAST 3.0': 3,
        'VAST 1.0 Wrapper': 4,
        'VAST 2.0 Wrapper': 5,
        'VAST 3.0 Wrapper': 6,
    }

    def __init__(self, code='', name=''):
        super().__init__(CreativeSubtypes.CREATIVE_SUBTYPES, code=code, name=name)


class ClickTypes(ConstantList):
    CLICK_TYPES = {
        'Non-Clickable': 1,
        'Clickable (details unknown)': 2
    }

    def __init__(self, list, code='', name=''):
        super().__init__(ClickTypes.CLICK_TYPES, code, name)


class RatioBasedSizeDescriptors(ConstantList):
    RATIO_BASED_SIZE_DESCRIPTORS = {
        'X Small': 1,
        'Small': 2
    }

    def __init__(self, code='', name=''):
        super().__init__(RatioBasedSizeDescriptors.RATIO_BASED_SIZE_DESCRIPTORS, code, name)
