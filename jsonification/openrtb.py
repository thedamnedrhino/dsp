from marshmallow import fields

from jsonification.common import Schema

import jsonification.adcom.context as adcom_context
import jsonification.adcom.request as adcom_request
import jsonification.adcom.response as adcom_response

import openrtb.openrtb as ortb


class ItemDomainSchema(Schema):
    """
    all fields mapped
    """
    ad = fields.Nested(adcom_request.AdSchema(), missing=None)
    site = fields.Nested(adcom_context.SiteSchema(), missing=None)
    content = fields.Nested(adcom_context.ContentSchema, missing=None)

    def get_base_instance(self):
        return ortb.ItemDomain()


class BidDomainSchema(Schema):
    """
    all fields mapped
    """
    ad = fields.Nested(adcom_response.AdSchema, missing=None)

    def get_base_instance(self):
        return ortb.BidDomain()


class ItemSchema(Schema):
    """
    missing fields: flrcur, seq, pmp
    """
    id = fields.Str()
    qty = fields.Int()
    flr = fields.Float()
    domain = fields.Nested(ItemDomainSchema, missing=None)
    ext = fields.Dict()

    def get_base_instance(self):
        return ortb.Item()


class OfferSchema(Schema):
    """
    represents the goods being auctioned in the request
    """
    item = fields.Nested(ItemSchema, many=True, missing=None)
    package = fields.Int(missing=0)
    dburl = fields.String(allow_none=True, missing=None)
    ext = fields.Dict()

    def get_base_instance(self):
        return ortb.Offer()


class BidRequestSchema(Schema):
    """
    missing fields: seats, wseats, curs, wcurs, source
    """
    id = fields.Str()
    test = fields.Bool()
    tmax = fields.Int()
    at = fields.Int()
    offer = fields.Nested(OfferSchema, missing=None)
    ext = fields.Dict()

    def get_base_instance(self):
        return ortb.BidRequest()


class PmpSchema(Schema):
    """
    not completed
    """
    private = fields.Int(missing=0)


class DealSchema(Schema):
    """
    not completed
    """
    pass


class BidSchema(Schema):
    """
    missing fields: tactic
    """
    id = fields.Str(missing=None)
    item = fields.Str(missing=None)
    deal = fields.Str(missing=None)
    price = fields.Int(missing=None)
    cid = fields.Str(missing=None)
    nurl = fields.Str(allow_none=True)
    burl = fields.Str(allow_none=True)
    lurl = fields.Str(allow_none=True)
    domain = fields.Nested(BidDomainSchema, missing=None)
    ext = fields.Dict()

    def get_base_instance(self):
        return ortb.Bid(None)


class SeatbidSchema(Schema):
    """
    all fields mapped
    """
    seat = fields.Str()
    package = fields.Int()
    bid = fields.Nested(BidSchema, many=True, missing=None)
    ext = fields.Dict()

    def get_base_instance(self):
        return ortb.Seatbid()


class BidResponseSchema(Schema):
    """
    missing fields: cur, cdata
    """
    id = fields.Str(missing=None)
    bidid = fields.Str(missing=None)
    nbr = fields.Int(missing=None)
    seatbid = fields.Nested(SeatbidSchema, many=True, allow_none=True)
    ext = fields.Dict()

    def get_base_instance(self):
        return ortb.BidResponse(None, None)


class OpenRtbSchema(Schema):
    """
    all fields mapped
    """
    ver = fields.Str()
    domainspec = fields.Str()
    domainver = fields.Str()
    request = fields.Nested(BidRequestSchema(), allow_none=True)
    response = fields.Nested(BidResponseSchema(), allow_none=True)
    ext = fields.Dict()

    def get_base_instance(self):
        return ortb.OpenRtb()
