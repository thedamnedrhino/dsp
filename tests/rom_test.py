from persistence import *
import pytest
from rom import util

@pytest.fixture
def client():
    util.set_connection_settings(port='3000')
    util.CONNECTION.flushdb()
    return 'a'

class TestRom():
    """
    A test to make sure the rom (redis orm) package works.
    We previously had problems with updating/deleting campaigns.
    See persistence.Campaign.__init__() for more details on the problem.
    """
    def test_rom(self, client):
        c = Campaign(id='1', seat='seat', bid_price=10, ads=[], advertiser_domain='asdfs')
        c.save()
        c = Campaign.query.all()[0]
        c.delete()