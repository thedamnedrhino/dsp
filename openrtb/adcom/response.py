import openrtb.default_params as default_params


class Ad(object):

    def __init__(self) -> None:
        self.sadid = ''  # see dadid
        self.dadid = ''  # a unique identifier for the ad, either dadid or sadid MUST be defined
        self.creative = Creative()
        self.audit = None  # instance of an Audit object (which we don't need right now; used for ad reviewing purposes and stuff)
        self.events = []  # array of event objects, see Event TODo not used right now
        self.adomain = ''  # the advertiser domain (e.g: ford.com)
        self.cat = []  # content categories of the add see adcom.constants
        self.lang = 'FA'  # language of the creative


class Creative(object):

    def __init__(self):
        self.video = Video()
        self.attr = []  # set of attributes describing the creative, see adcom.constants.CreativeAttributes


class Video(object):

    def __init__(self) -> None:
        self.subtype = 1  # the subtype of the video creative, see adcom.constants.VideoSubtypes
        self.adm = ''  # the ad markup see self.curl
        self.curl = ''  # the url at which the ad markup can be found, applicable for VAST and other creative types
        self.mimes = []  # the ad mime types, (don't why there would be more than one)


class Event(object):  # the event classes are not used right now

    def __init__(self):
        self.event = 1  # type of event to track. REQUIRED. see adcom.constants.Events TODo not used
        self.method = 1  # type of tracking requested. REQUIRED. see adcom.constants.EventTrackingMethods TODo not used
        self.url = ''  # the url of the image or js that must be called for the image or js tracking to work
        self.customdata = {}  # array sent with the request to the tracking url
