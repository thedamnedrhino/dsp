from marshmallow import fields
from jsonification.common import Schema

from persistence import AdcomResponseAd as Ad, AdcomResponseCreative as Creative, AdcomResponseVideo as Video
from openrtb.adcom.response import Event

class VideoSchema(Schema):
    """
    all fields mapped
    """
    subtype = fields.Int(missing=None)
    adm = fields.Str(missing=None)
    curl = fields.Str(missing=None)
    mimes = fields.List(fields.Str(), missing=None)

    def get_base_instance(self):
        return Video()


class CreativeSchema(Schema):
    """
    all fields mapped
    """
    video = fields.Nested(VideoSchema(), missing=None)
    attr = fields.List(fields.Str())

    def get_base_instance(self):
        return Creative()


class EventSchema(Schema):  # the event classes are not used right now
    """
    all fields mapped
    """
    event = fields.Int(missing=None)
    method = fields.Int(missing=None)
    url = fields.Str(missing=None)
    customdata = fields.Dict(missing=None)

    def get_base_instance(self):
        return Event()


class AdSchema(Schema):
    """
    missing fields: audit
    """
    sadid = fields.Str(missing=None)
    dadid = fields.Str(missing=None)
    creative = fields.Nested(CreativeSchema(), allow_none=False)
    events = fields.Nested(EventSchema(), missing=None)
    adomain = fields.Str(missing=None)
    cat = fields.List(fields.Str(), missing=None)
    lang = fields.Str(missing=None)

    def get_base_instance(self):
        return Ad()
