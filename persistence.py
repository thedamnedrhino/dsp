from typing import Iterable, Collection, Optional, Dict, List
import openrtb.adcom.response
import rom
import dsp
import pdb


class Campaign(dsp.Campaign, rom.Model):
    zakhar_id = rom.PrimaryKey()
    id = rom.Text(unique=True, index=True, keygen=rom.IDENTITY)
    seat = rom.Text(required=True)
    bid_price = rom.Integer(required=True)
    ads = rom.OneToMany('CampaignAd', column='campaign')
    advertiser_domain = rom.Text()
    advertiser_categories = rom.Json(default=[])
    site_domains = rom.Json(default=[])
    site_domains_black_list = rom.Boolean(default=True)
    site_categories = rom.Json(default=[])
    site_categories_black_list = rom.Boolean(default=True)
    video_categories = rom.Json(default=[])
    video_categories_black_list = rom.Boolean(default=True)
    enabled = rom.Boolean(default=True)

    def __init__(self, **kwargs):
        """
        we previously had a constructor with roughly (defaults were added
        to ALL arguments and a **kwargs was added at the end) the same signature as dsp.Campaign.
        But that caused errors on updating campaigns because of the `ads` property
        and its constructor argument default.
        :param kwargs: all the keyword arguments in dsp.Campaign are supported
        """
        rom.Model.__init__(self, **kwargs)
        self.initialize()




class CampaignAd(dsp.CampaignAd, rom.Model):
    zakhar_id = rom.PrimaryKey()
    campaign = rom.ManyToOne('Campaign', on_delete='cascade')
    adcom = rom.OneToOne(ftable='AdcomResponseAd', on_delete='cascade') # todo create adcom rom model

    def __init__(self, campaign: Optional['Campaign'] = None, adcom: Optional['AdcomResponseAd'] = None, **kwargs):
        rom.Model.__init__(self, campaign=campaign, adcom=adcom, **kwargs)
        dsp.CampaignAd.__init__(self, campaign, adcom)

    def set_campaign(self, c: Campaign):
        self.campaign = c


class AdcomResponseAd(openrtb.adcom.response.Ad, rom.Model):
    zakhar_id = rom.PrimaryKey()
    dadid = rom.Text()
    sadid = rom.Text()
    creative = rom.OneToOne(ftable='AdcomResponseCreative', on_delete='cascade')
    adomain = rom.Text()
    cat = rom.Json()

    def __init__(self, **kwargs):
        rom.Model.__init__(self, **kwargs)


class AdcomResponseCreative(openrtb.adcom.response.Creative, rom.Model):
    zakhar_id = rom.PrimaryKey()
    video = rom.OneToOne(ftable='AdcomResponseVideo', on_delete='cascade')

    def __init__(self, **kwargs):
        rom.Model.__init__(self, **kwargs)


class AdcomResponseVideo(openrtb.adcom.response.Video, rom.Model):
    zakhar_id = rom.PrimaryKey()
    subtype = rom.Integer()
    adm = rom.Text()
    curl = rom.Text()

    def __init__(self, **kwargs):
        rom.Model.__init__(self, **kwargs)


