from flask import Flask, request
import flask.wrappers
from jsonification.openrtb import OpenRtbSchema
from jsonification.dsp import CampaignSchema, CampaignAdSchema
from persistence import Campaign
from openrtb.openrtb import OpenRtb
import dsp
from redis import Redis
import pdb

app = Flask(__name__)


class CampaignDoesntExistError(Exception):
    def __init__(self, campaign_id):
        super().__init__('campaign with the id "{}" does not exist'.format(campaign_id))
        self.campaign_id = campaign_id


def init():
    if 'REDIS_PORT' in app.config:
        from rom import util
        util.set_connection_settings()
        util.set_connection_settings(port=app.config['REDIS_PORT'])


@app.route('/bid_request', methods=['POST'])
def bid_request():
    init()
    json_bid = request.get_data(as_text=True)
    s = OpenRtbSchema()
    result = s.loads(json_bid)
    rtb, errors = result
    if len(errors) > 0:
        return '{}'.format(errors)
    else:
        bid_response = dsp_response(rtb)
        ortb = OpenRtb()
        ortb.response = bid_response
        string, errors = s.dumps(ortb)
        if errors:
            raise_serialization_error('OpenRTB', errors)
        return s.dumps(ortb).data


@app.route('/campaign/new', methods=['POST'])
def new_campaign():
    init()
    json_campaign = request.get_data(as_text=True)
    s = CampaignSchema()
    result = s.loads(json_campaign)
    campaign, errors = result
    if len(errors) > 0:
        raise Exception('{}'.format(errors))
    else:
        campaign_dict = request.get_json(force=True)
        ads = campaign_dict['ads']
        s = CampaignAdSchema()
        ads, errors = s.load(ads, many=True)
        if len(errors) > 0:
            raise Exception('Error loading ads: "{}"'.format(errors))
        for ad in ads: # this part is a bit sketchy. Had to use workarounds for the circular reference
            ad.set_campaign(campaign)
            ad.save()
        campaign.save()
        return str(len(Campaign.query.all()))


@app.route('/campaign/<campaign_id>', methods=['GET'])
def get_campaign(campaign_id):
    init()
    try:
        campaign = find_single_campaign(campaign_id)
    except CampaignDoesntExistError as e:
        return flask.wrappers.Response(status=404, response='campaign with id "{}" does not exist'.format(e.campaign_id))
    s = CampaignSchema()
    string, errors = s.dumps(campaign)
    if errors:
        raise_serialization_error('Campaign', errors)
    return string


@app.route('/campaign/<campaign_id>/enable', methods=['POST'])
def enable_campaign(campaign_id):
    init()
    set_campaign_enabled(campaign_id, True)
    return flask.wrappers.Response('success')


@app.route('/campaign/<campaign_id>/disable', methods=['POST'])
def disable_campaign(campaign_id):
    init()
    set_campaign_enabled(campaign_id, False)
    return flask.wrappers.Response('success')


@app.route('/campaign/<campaign_id>/delete', methods=['POST'])
def delete_campaign(campaign_id):
    init()
    campaign = find_single_campaign(campaign_id)
    campaign.delete()
    return flask.wrappers.Response('success')


@app.route('/campaign/', methods=['GET'])
def get_campaign_list():
    init()
    campaigns = Campaign.query.all()
    id_list = [c.get_id() for c in campaigns]
    import json
    response = flask.wrappers.Response(response=json.dumps(id_list, ensure_ascii=False), mimetype='application/json')
    return response


@app.route('/campaign/enabled', methods=['GET'])
def get_enabled_campaigns_list():
    init()
    campaigns = Campaign.query.all()
    id_list = []
    for c in campaigns:
        if c.is_enabled():
            id_list.append(c.get_id())
    import json
    response = flask.wrappers.Response(response=json.dumps(id_list, ensure_ascii=False), mimetype='application/json')
    return response


def find_single_campaign(campaign_id: str) -> Campaign:
    campaigns = Campaign.query.filter(id=campaign_id).all()
    if len(campaigns) > 1:
        raise Exception('there should only one campaign with the id "{}", "{}" campaigns found'.format(campaign_id, len(campaigns)))
    elif len(campaigns) == 0:
        raise CampaignDoesntExistError(campaign_id)
    campaign = campaigns[0]
    return campaign


def set_campaign_enabled(campaign_id: str, enabled: bool):
    campaign = find_single_campaign(campaign_id)
    campaign.enable() if enabled else campaign.disable()
    campaign.save(full=True)


def raise_serialization_error(object_type: str, errors):
    raise Exception('there were errors serializing the "{}" object to json. errors: {}'.format(object_type, errors))

def raise_deserializition_error(serialization: str, object_type: str, errors):
    raise Exception('there were errors unserializing the string "{}" into an object of type "{}"'
                    'errors: {}'.format(serialization, object_type, errors))

def dsp_response(rtb: OpenRtb):
    campaigns = Campaign.query.all()
    d = dsp.DSP(campaigns)
    bid_request = rtb.request
    if request is None:
        raise Exception('the OpenRtb request should contain a valid BidRequest object')
    bid_response = d.bid(dsp.WrappedORTBRequest(bid_request))
    bid_response = bid_response.ORTBResponse()
    return bid_response
